package com.example.root.projeck;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.Button;
import android.widget.CheckBox;
import android.view.View;
import android.content.Intent;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //private RelativeLayout relative1relative2,relative3,relative4,relative5;
    private CheckBox cek1,cek2,cek3,cek4,cek5;
    private Button tombol;
    int a,b,c,d,e,hasil;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fungsi();

    }
    public void fungsi(){
        tombol = (Button) findViewById(R.id.total);
        tombol.setOnClickListener(this);

        cek1 = (CheckBox) findViewById(R.id.check1);
        cek1.setOnClickListener(this);
        cek2 = (CheckBox) findViewById(R.id.check2);
        cek2.setOnClickListener(this);
        cek3 = (CheckBox) findViewById(R.id.check3);
        cek3.setOnClickListener(this);
        cek4 = (CheckBox) findViewById(R.id.check4);
        cek4.setOnClickListener(this);
        cek5 = (CheckBox) findViewById(R.id.check5);
        cek5.setOnClickListener(this);

        a = 500;
        b = 100;
        c = 200;
        d = 1000;
        e = 50;
        hasil = 0;

    }
    public void onClick(View v){

        if(v == cek1){
            if(cek1.isChecked()) {
                hasil = hasil + a;
            }else{
                hasil = hasil - a;
            }
        }
        if(v == cek2){
            if(cek2.isChecked()) {
                hasil = hasil + b;
            }else{
                hasil = hasil - b;
            }
        }
        if(v == cek3){
            if(cek3.isChecked()) {
                hasil = hasil + c;
            }else{
                hasil = hasil - c;
            }
        }if(v == cek4){
            if(cek4.isChecked()) {
                hasil = hasil + d;
            }else{
                hasil = hasil - d;
            }
        }if(v == cek5){
            if(cek5.isChecked()) {
                hasil = hasil + e;
            }else{
                hasil = hasil - e;
            }
        }
        if(v == tombol){
            Toast.makeText(getApplication(),"hasil"+hasil,Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), main2.class);
            intent.putExtra("total",String.valueOf(hasil));
            startActivity(intent);
        }
    }
}
