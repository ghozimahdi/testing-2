package com.example.root.projeck;
import android.content.Intent;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by root on 14/06/16.
 */
public class main2 extends AppCompatActivity{
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);

        Intent intent = getIntent();
        String data = intent.getStringExtra("total");
        TextView total = (TextView)findViewById(R.id.total);
        total.setText(data);
    }
}
